#!/bin/bash

git fetch --all
chmod +x TS3MusicBot_install/*
tar cfv ../TS3MusicBot_install.tar TS3MusicBot_install
cp TS3MusicBot_install.sh ../
git reset --hard
git checkout release_master
if [ -f TS3MusicBot_install.tar ]; then rm TS3MusicBot_install.tar; fi
if [ -f TS3MusicBot_install.sh ]; then rm TS3MusicBot_install.sh; fi
mv ../TS3MusicBot_install.tar TS3MusicBot_install.tar
mv ../TS3MusicBot_install.sh TS3MusicBot_install.sh
git add TS3MusicBot_install.tar TS3MusicBot_install.sh
git commit -m '[skip ci] commit from CI runner'
git push --follow-tags origin release_master

exit 0