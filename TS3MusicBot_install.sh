#!/bin/bash

# Description:
#	This install Script install all needed Packages for the TS3MusicBot.

# Links
PROJECT_SIDE="https://gitlab.com/philw95/ts3musicbot_install"
ARCHIV_NAME="TS3MusicBot_install.tar"
INSTALL_ARCHIV="${PROJECT_SIDE}/raw/release_master/${ARCHIV_NAME}"

cd /tmp

# Operating System
LINUX_VERSION=$(cat /etc/issue)
LINUX_VERSION2=$(cat /etc/*-release)

# Define colors for printf
MAIN='\033[0m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
FLASH='\033[5m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

# Zeichen
GOOD="[${GREEN}✓${MAIN}]"
ERROR="[${RED}✗${MAIN}]"
NEUTRAL="[${YELLOW}-${MAIN}]"
INFO="[i]"

# function
function check_exit {
if ! [ "$?" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: ${1} Exit state: ${2} \n"
	exit 1
fi
}

function delete_files {
cd /tmp

printf " ${INFO} Removing Setup files \n"
NO_OUTPUT="1"
spinner & spinner_background "rm -R TS3MusicBot_install/" "rm"
NO_OUTPUT=""
echo
}

function good_edit {
printf "\033[s\033[1A\r\033[1C${GOOD}\033[u"
}

function error_edit {
printf "\033[s\033[1A\r\033[1C${ERROR}\033[u"
}

function bold {
printf "${BOLD}${1}${MAIN}"
}

function spinner {
while true
do
  printf "\033[s\033[1A\r\033[2C${YELLOW}\\\\${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}|${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}/${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}-${MAIN}\033[u" ; sleep 0.15
done
}

function spinner_background {

PID="$!"
if [ "${NO_OUTPUT}" = "1" ]; then
	${1} &> /dev/null
else
	${1}
fi
EXITSTATE="$?"
if ! [ "${EXITSTATE}" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: '${2}' Exit state: ${EXITSTATE} \n"
	kill $PID
	wait $PID 2> /dev/null
	exit 1
fi
good_edit
kill $PID
wait $PID 2> /dev/null

}
# Example for spinner:
# spinner & spinner_background "apt-get update" "apt-get update"

# Export functions and Variables
function export_variables {
# Functions
export -f check_exit good_edit error_edit bold spinner spinner_background delete_files
# Variables
export GOOD ERROR NEUTRAL INFO
export LINUX_VERSION LINUX_VERSION2
}

# Banner
clear
printf "#  _____ ____ _____ __  __           _      ____        _    #\n"
printf "# |_   _/ ___|___ /|  \/  |_   _ ___(_) ___| __ )  ___ | |_  #\n"
printf "#   | | \___ \ |_ \| |\/| | | | / __| |/ __|  _ \ / _ \| __| #\n"
printf "#   | |  ___) |__) | |  | | |_| \__ \ | (__| |_) | (_) | |_  #\n"
printf "#   |_| |____/____/|_|  |_|\__,_|___/_|\___|____/ \___/ \__| #\n"
printf "#   ___           _        _ _ ____            _       _     #\n"
printf "#  |_ _|_ __  ___| |_ __ _| | / ___|  ___ _ __(_)_ __ | |_   #\n"
printf "#   | || '_ \/ __| __/ _\` | | \___ \ / __| '__| | '_ \| __|  #\n"
printf "#   | || | | \__ \ || (_| | | |___) | (__| |  | | |_) | |_   #\n"
printf "#  |___|_| |_|___/\__\__,_|_|_|____/ \___|_|  |_| .__/ \__|  #\n"
printf "#                                               |_|          #\n"

echo
if [ "$(id -u)" = "0" ]; then
        printf " ${GOOD} Root User \n"
		echo
else
        printf " ${ERROR} Not Root User! You must have Root Rights! Use 'su' or 'sudo -i' \n"
        exit 1
fi

printf " ${INFO} Download Setup files Archiv \n"
NO_OUTPUT="1"
spinner & spinner_background "wget ${INSTALL_ARCHIV}" "wget"
NO_OUTPUT=""

printf " ${INFO} Extracting Setup files \n"
NO_OUTPUT="1"
spinner & spinner_background "tar xfv ${ARCHIV_NAME}" "tar"
NO_OUTPUT=""

printf " ${INFO} Removing Setup files Archiv \n"
NO_OUTPUT="1"
spinner & spinner_background "rm ${ARCHIV_NAME}" "rm"
NO_OUTPUT=""
echo

cd TS3MusicBot_install
export_variables
exec ./TS3MusicBot_install

delete_files

exit 0