# TS3MusicBot Control Script

[![license](https://img.shields.io/badge/License-MIT_License-red.svg)](LICENSE) [![pipeline status master](https://gitlab.com/philw95/ts3musicbot_install/badges/master/pipeline.svg)](https://gitlab.com/philw95/ts3musicbot_install/commits/master)

### TS3MusicBot Homepage: https://ts3musicbot.net
[![TS3MusicBot_Logo](http://ts3musicbot.net/linkus/ts3musicbot1.png)](https://www.ts3musicbot.net)  [![TS3MusicBot_Logo2](http://ts3musicbot.net/linkus/TS3MusicBot_logo.gif)](https://www.ts3musicbot.net)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/W7W814GV6)
---

# Table of Content

### [1. Attention](#1-attention-)

### [2. About](#2-about-)

### [3. Usage](#3-usage-)

### [4. Supportet OS](#4-supportet-operating-systems-)

### [5. Support](#5-support-)

---
&nbsp;  

# 1. Attention [↑](#table-of-content)

**A license is required to use the Bot! Buy license here: https://ts3musicbot.net**

---
&nbsp;  

# 2. About [↑](#table-of-content)

TS3MusicBot.net:
> You ever wanted to play music on your teamspeak or discord server? Now you can!
> TS3MusicBot is a unique feature for your teamspeak or discord server fully working on linux and windows.
> 
> Upload music files, manage folders, play all kind of music files, stream live internet radio stations, direct playback of youtube, soundcloud and more links.
> The TS3MusicBot can be controlled with chat commands or with the build in webinterface.
> 
> Listen to music in groups while playing your favorite game. Let your friends listen to a youtube video you found.
> A new experience to listen music live with others in the same channel.

This Script:

**Basic installation for TS3MusicBot in:**

- Portable Mode (Easy)
- Notportable Mode (Advanced)

After Installation you can Install TS3MusicBot_Control: https://gitlab.com/philw95/ts3musicbot_control over this Install Script

---
&nbsp;  

# 3. Usage [↑](#table-of-content)

Use this Command:
```
bash <(curl -sSL http://y.pkhw.de/ts3mbinstall)
```

**It must be started with root rights**

---
&nbsp;  

# 4. Supportet Operating Systems [↑](#table-of-content)

| Debian | Ubuntu | Cent OS |
|:------:|:------:|:-------:|
|   11   |  20.04 |    8    |
|   10   |  18.04 |         |
|    9   |        |         |

---
&nbsp;  

# 5. Support [↑](#table-of-content)

### TS3MusicBot Support:

[![Support](https://xbackbone.pkhw.cloud/36ed1/qOKoWOYI76.png/raw.png)](https://support.ts3musicbot.net/)
<a href="https://ts3musicbot.net"><img src="https://xbackbone.pkhw.de/36ed1/hANArIWozi187.png/raw" width="70" title="Website" alt="Website"></a>
<a href="mailto:support@ts3musicbot.de"><img src="https://xbackbone.pkhw.de/36ed1/nijAKIgEYA923.png/raw" width="70" title="Send E-Mail" alt="E-Mail"></a>

---
&nbsp;  